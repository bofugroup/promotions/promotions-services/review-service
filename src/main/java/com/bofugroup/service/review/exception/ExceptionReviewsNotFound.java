package com.bofugroup.service.review.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Not Found Review")
public class ExceptionReviewsNotFound extends RuntimeException
{
	private static final long serialVersionUID = 1L; 
}
