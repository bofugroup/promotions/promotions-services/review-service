package com.bofugroup.service.review.facade.mapper.Imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bofugroup.service.review.business.dto.Reviews;
import com.bofugroup.service.review.facade.dto.DTOinReviews;
import com.bofugroup.service.review.facade.dto.DTOoutReviews;
import com.bofugroup.service.review.facade.mapper.IRewviewsMapper;

@Component("ReviewsMapper")
public class ReviewsMapper implements IRewviewsMapper{


	@Override
	public Reviews mapInReview(DTOinReviews reviewRespon) {
		Reviews review = new Reviews();
		if(reviewRespon != null) {
			review.setCommentary(reviewRespon.getCommentary());
			review.setCommentary_date(reviewRespon.getCommentary_date());			
		}
		return review;
	}
	
	@Override
	public DTOoutReviews mapOutReview(Reviews review) {
		DTOoutReviews response = new DTOoutReviews();
		if (review != null) {
			response.setId(review.getId());
			response.setCommentary(review.getCommentary());
			response.setCommentary_date(review.getCommentary_date());;
		}
		return response;
	}
	
	@Override
	public List<DTOoutReviews> mapOutListReviews(List<Reviews> review)
	{
		
		List<DTOoutReviews> response = new ArrayList<DTOoutReviews>();
		
		if(review != null && !review.isEmpty()) 
		{
			for (Reviews temp : review) 
			{	
				response.add(mapOutReview(temp));
			}
		}
		return response;
	}
}
