package com.bofugroup.service.review.facade.mapper;

import java.util.List;

import com.bofugroup.service.review.business.dto.Reviews;
import com.bofugroup.service.review.facade.dto.DTOinReviews;
import com.bofugroup.service.review.facade.dto.DTOoutReviews;

public interface IRewviewsMapper 
{	
	DTOoutReviews mapOutReview(Reviews review);
	Reviews mapInReview(DTOinReviews reviewRespon);
	List<DTOoutReviews> mapOutListReviews(List<Reviews> review);
}
