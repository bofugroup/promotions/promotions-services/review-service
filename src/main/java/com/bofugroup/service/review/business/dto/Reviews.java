package com.bofugroup.service.review.business.dto;


import java.sql.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="reviews_tbl")
public class Reviews{
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The Database generated product ID")
	Long id;
	@ApiModelProperty(notes = "The Commentary of Review")
	@Column(name = "commentary", nullable = false)
	String commentary;
	@ApiModelProperty(notes = "The date commentary of Review")
	@Column(name = "commentary_date", nullable = false)
	Date commentary_date;
	
	public Reviews() {}

	public Reviews(String commentary, Date commentary_date) {
		super();
		this.commentary = commentary;
		this.commentary_date = commentary_date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public Date getCommentary_date() {
		return commentary_date;
	}

	public void setCommentary_date(Date commentary_date) {
		this.commentary_date = commentary_date;
	}

}
